# EEE3088F PiHat Project

The goal is to design a microPi HAT compliant PCB that can be used as a low-cost audio detection device on the Raspberry Pi. 

The device will have three modes of operation that can be toggled by a button.One is to measure low-levels of sound in a room, another for measuring high-levels of sound in a room and the last mode is to ensure the sound detected falls within a desired range(i.e acceptable valume for talking that isn't too high or too low). The device could be used for volume control in a corporate environment where employees on a call could monitor their input and ensure they are not talking loudly or softly. In an industrial setting, a factory floor could use the device to measure ambient sound and ensure that should it exceed a threshold, the employees and employers know that personal protective equipment needs to be used. This device could also be used to ensure the volume in a room remains below a certain level.
